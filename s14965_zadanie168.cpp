﻿#include <iostream>
#include <conio.h>

using namespace std;
bool sprawdzenie(int a, int b, int c);  //funkcja sprawdzająca czy figra nie wykracza poza obszar rysowania
void rysuj(int y, int x, int r, char z);
void manipulacja(int &a, int &b, int &c);
void wczytaj_rozmiar(int &r);

int main()
{
	int	kolumna;
	int wiersz;
	int rozmiar;
	char znak;						//zmienna użyta do zapamiętania znaku do wypisania figury
	bool s;						//zmienna użyta do sprawdzenie czy figura nie wykracza poza obszar rysowania

	while (1)
	{
		kolumna = 1;
		wiersz = 1;

		cout << "Wprowadź znak ASCII: ";
		cin >> znak;		cout << endl;
		wczytaj_rozmiar(rozmiar);		cout << endl;

		s = sprawdzenie(kolumna, wiersz, rozmiar);
		if (s == true)
		{
			while (1)
			{

				cout << endl;
				rysuj(wiersz, kolumna, rozmiar, znak);
				cout << endl; cout << "(+)Powieksz	(-)Pomniejsz	Przesun za pomoc strzalek" << endl;
				manipulacja(wiersz, kolumna, rozmiar);
			}
		}
	}
	return 0;
}

void wczytaj_rozmiar(int &r)
{

	cout << "Wprowadź rozmiar(1-50): ";
	cin >> r;

	while (cin.fail() || (r <= 0 || r > 50))
	{
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		cout << "Niepoprawna wartosc." << endl;
		cout << "Wprowadź rozmiar(1-50): ";
		cin >> r;
	}
	cin.ignore(numeric_limits<streamsize>::max(), '\n');
}

bool sprawdzenie(int a, int b, int c)
{
	int p, l, g, d;
	g = b;
	l = a;
	d = (2 * c) + b;
	p = c + a;

	if (g>0 && d <102 && l > 0 && p <100 && c>0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void rysuj(int y, int x, int r, char z)
{
	char tabela[101][101];
	int i, j;

	for (i = 0; i < 101; i++)					//zapobieganie śmieciom na ekranie, poprzez wypełnienie tabeli znakiem spacji
	{
		tabela[i][0] = '/';
		if (i == 0)
		{
			for (j = 1; j < 101; j++)
			{
				tabela[i][j] = '-';
			}
		}
		else if (i<100)
		{
			for (j = 1; j < 101; j++)
			{
				tabela[i][j] = ' ';
			}
		}
		else if (i == 100)
		{
			for (j = 1; j < 101; j++)
			{
				tabela[i][j] = '-';
			}
		}
		tabela[i][j - 2] = '/';
	}


	for (i = 0; i < r; i++)					//rysowanie strzałki #1
	{
		tabela[y][x] = z;
		x++;
		y++;
	}
	x = x - 2;
	for (i = 0; i < r - 1; i++)				//rysowanie strzałki #2
	{
		tabela[y][x] = z;
		y++;
		x--;
	}

	for (i = 0; i < 101; i++)					//wypisywanie tabeli
	{
		for (j = 0; j < 100; j++)
		{
			cout << tabela[i][j] << " ";
		}
		cout << endl;
	}

}

void manipulacja(int &a, int &b, int &c)
{

	bool i;
	unsigned char zmiana;			//znak do "manipulacji" "rysunkiem"
	zmiana = _getch();
	switch (zmiana)
	{
	case 72: //przesunięcie w górę
	{
		a--;
		i = sprawdzenie(b, a, c);
		if (i == false)
		{
			a++;
			system("CLS");
			cout << "Nie mozna przesunac wyzej";
			break;
		}
		else if (i == true)
		{
			system("CLS");
			break;
		}
	}
	case 80: //przesunięcie w dół
	{
		a++;
		i = sprawdzenie(b, a, c);
		if (i == false)
		{
			a--;
			system("CLS");
			cout << "Nie mozna przesunac nizej";
			break;
		}
		else if (i == true)
		{
			system("CLS");
			break;
		}
	}
	case 75: //przesunięcie w lewo
	{
		b--;
		i = sprawdzenie(b, a, c);
		if (i == false)
		{
			b++;
			system("CLS");
			cout << "Nie mozna przesunac bardziej w lewo";
			break;
		}
		else if (i == true)
		{
			system("CLS");
			break;
		}
	}
	case 77: //przesunięcie w prawo
	{
		b++;
		i = sprawdzenie(b, a, c);
		if (i == false)
		{
			b--;
			system("CLS");
			cout << "Nie mozna przesunac bardziej w prawo";
			break;
		}
		else if (i == true)
		{
			system("CLS");
			break;
		}
	}
	case '+': //powiększenie
	{
		c++;
		i = sprawdzenie(b, a, c);
		if (i == false)												//program nie powinien uniemożliwiać powiększenia obrazka ze względu na jego położenie, dlatego ta opcja ma dodatkowe sprawdzenia
		{															// i przesunie obrazek jeśli będzie to wymagane by go powiększyć, oczywiście nie łamiąc przy tym ograniczeń
			switch (c)
			{
			case 51:
			{
				c = 50;
				system("CLS");
				cout << "Nie mozna bardziej powiekszyc";
				break;
			}
			default:
			{
				if (a > 2 && b > 2)
				{
					a = a - 2;
					b = b - 2;
					system("CLS");
				}
				else if (a > 2 && b == 2)
				{
					a = a - 2;
					b--;
					system("CLS");
				}
				else if (a > 2 && b == 1)
				{
					a = a - 2;
					system("CLS");
				}
				else if (a == 2 && b > 2)
				{
					a--;
					b = b - 2;
					system("CLS");
				}
				else if (a == 1 && b > 2)
				{
					b = b - 2;
					system("CLS");
				}
				else if (a == 2 && b == 2)
				{
					a--;
					b--;
					system("CLS");
				}
				else if (a == 2 && b == 1)
				{
					a--;
					system("CLS");
				}
				else if (a == 1 && b == 2)
				{
					b--;
					system("CLS");
				}
			}
			}
		}
		else if (i == true)
		{
			system("CLS");
		}
	}break;
	case '-': //pomniejszenie
	{
		c--;
		i = sprawdzenie(b, a, c);
		if (i == false)
		{
			c++;
			system("CLS");
			cout << "Nie mozna bardziej zmniejszyc";
			break;
		}
		else if (i == true)
		{
			system("CLS");
			break;
		}
	}
	default:
	{
		system("cls");
		break;
	}
	}
}